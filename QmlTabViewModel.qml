import QtQuick 2.0
import QtQuick.Controls 1.4

TableView {
    property alias tableView: tableView

    id: tableView
    TableViewColumn {title: qsTr("日期"); role: "0"; width: 96}
    TableViewColumn {title: qsTr("时间"); role: "1"; width: 96}
    TableViewColumn {title: qsTr("用户编号"); role: "2"; width: 96}
    TableViewColumn {title: qsTr("用户名称"); role: "3"; width: 96}
    TableViewColumn {title: qsTr("类型"); role: "4"; width: 96}
    TableViewColumn {title: qsTr("报警内容"); role: "5"; width: 96}
    TableViewColumn {title: qsTr("代码"); role: "6"; width: 96}
    TableViewColumn {title: qsTr("防区"); role: "7"; width: 96}
    TableViewColumn {title: qsTr("分区"); role: "8"; width: 96}
    TableViewColumn {title: qsTr("来电电话"); role: "9"; width: 96}
    TableViewColumn {title: qsTr("防区位置"); role: "10"; width: 96}
    model: theModel
    alternatingRowColors: false
    backgroundVisible: false
}
