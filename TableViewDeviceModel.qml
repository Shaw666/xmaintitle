﻿import QtQuick 2.7
import QtQuick.Controls 1.4

TableView {
    property alias tableView: tableView

    id: tableView
    TableViewColumn {title: qsTr("用户编号"); role: "0"; width: 96}
    TableViewColumn {title: qsTr("设备名称"); role: "1"; width: 96}
    TableViewColumn {title: qsTr("网络状态"); role: "2"; width: 96}
    TableViewColumn {title: qsTr("主机状态"); role: "3"; width: 96}
    TableViewColumn {title: qsTr("更新时间"); role: "4"; width: 96}
    TableViewColumn {title: qsTr("防区1"); role: "5"; width: 72}
    TableViewColumn {title: qsTr("防区2"); role: "6"; width: 72}
    TableViewColumn {title: qsTr("防区3"); role: "7"; width: 72}
    TableViewColumn {title: qsTr("防区4"); role: "8"; width: 72}
    TableViewColumn {title: qsTr("防区5"); role: "9"; width: 72}
    TableViewColumn {title: qsTr("防区6"); role: "10"; width: 72}
    TableViewColumn {title: qsTr("防区7"); role: "11"; width: 72}
    TableViewColumn {title: qsTr("防区8"); role: "12"; width: 72}
    TableViewColumn {title: qsTr("防区9"); role: "13"; width: 72}
    TableViewColumn {title: qsTr("防区10"); role: "14"; width: 72}
    TableViewColumn {title: qsTr("防区11"); role: "15"; width: 72}
    TableViewColumn {title: qsTr("防区12"); role: "16"; width: 72}
    TableViewColumn {title: qsTr("防区13"); role: "17"; width: 72}
    TableViewColumn {title: qsTr("防区14"); role: "18"; width: 72}
    TableViewColumn {title: qsTr("防区15"); role: "19"; width: 72}
    TableViewColumn {title: qsTr("防区16"); role: "20"; width: 72}

    model: deviceModel
    alternatingRowColors: false
    backgroundVisible: false
}
