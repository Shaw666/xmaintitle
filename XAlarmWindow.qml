﻿import QtQuick 2.0
import QtQuick.Controls 1.4
import QtGraphicalEffects 1.0
import QtQuick.Controls.Styles 1.4
// 0 - 隐藏 1 -预览 2- 全窗口
Rectangle {
    id: xAlarmWindow
    //    property int mainWidth: 1024
    property int mainHeight: 20
    property int qmlTableViewHeight: 0
    property int alarmWindowState: 0
    width: parent.width
    height : mainHeight

    Rectangle {
        id: alarmWindowBtns
        width: parent.width
        height: 20
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.bottom: parent.bottom
        color: "#4682B4"
        Button {
            id: preViewBtn
            width: 16
            height: 16
            iconSource: "image/up16x16.png"
            anchors.top: parent.top
            anchors.topMargin: 2
            anchors.right: parent.right
            anchors.rightMargin: 2
            style: ButtonStyle {
                background: Rectangle {
                    color:"#4682B4"
                }
            }
            onClicked: {
                preViewBtnClicked()
            }
        }
        Button {
            id: maxViewBtn
            width: 16
            height: 16
            iconSource: "image/arrawsalt32x32.png"
            anchors.top: parent.top
            anchors.topMargin: 2
            anchors.right: preViewBtn.left
            anchors.rightMargin: 2

            onClicked: {
                console.log("==============max alarm window")
                if(alarmWindowState === 3){
                    return;
                }
                alarmWindowState = 3
                qmlTableView.visible=true
                xAlarmWindow.mainHeight = mainWindow.height-mainTitle.height
                qmlTableViewHeight = xAlarmWindow.height-alarmWindowBtns.height
                preViewBtn.iconSource="image/down16x16.png"
            }
        }
    }
    Rectangle {
        height: qmlTableViewHeight
        width: parent.width
        color: "#FCFCFC"
        anchors.bottom: alarmWindowBtns.top
        QmlTabViewModel{
            id: qmlTableView
            height: parent.height
            width: parent.width
            visible: false
            tableView.itemDelegate:Rectangle {
                TextField{
                    id: textField
                    height: 22
                    text: styleData.value + styleData.column
                    selectByMouse: true
                    onEditingFinished: {
                        theModel.Set(styleData.row, styleData.column, textField.text);
                    }
                }
            }
            tableView.rowDelegate: Rectangle {
                height: 22
            }
        }
    }

    Component.onCompleted: {
        if(alarmWindowState === 0){
            xAlarmWindow.mainHeight = 20
        }else if(alarmWindowState === 1){
            xAlarmWindow.mainHeight = 240
        }else if (alarmWindowState === 2) {
            xAlarmWindow.mainHeight = parent.height-mainTitle.height-navWindow.height
        }
    }
    function preViewBtnClicked(){
        if(alarmWindowState === 0) {
            qmlTableViewHeight=220
            mainHeight=20 + qmlTableViewHeight
            qmlTableView.visible=true
            alarmWindowState = 1
            preViewBtn.iconSource="image/down16x16.png"
        }else if(alarmWindowState === 1 || alarmWindowState === 3) {
            qmlTableViewHeight=0
            mainHeight=20
            qmlTableView.visible=false
            alarmWindowState = 0
            preViewBtn.iconSource="image/up16x16.png"
        }
    }
}


