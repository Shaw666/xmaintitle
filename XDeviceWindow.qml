﻿import QtQuick 2.7
import QtQuick.Controls 1.4
import QtGraphicalEffects 1.0
import QtQuick.Controls.Styles 1.4
Rectangle {
    property int pageId: 0
    property bool isvisible: false
    id: xDeviceWindow
    color: "#FCFCFC"
    width: parent.width
    height: parent.height
    anchors.top: parent.top
    anchors.left: parent.left
    visible: isvisible
    TableViewDeviceModel {
        id: tableviewDeviceModel
        height: parent.height
        width: parent.width
        visible: true
        tableView.itemDelegate:Rectangle {
            TextField{
                id: textField
                height: 22
                text: styleData.value
                selectByMouse: true
                onEditingFinished: {
                    deviceModel.Set(styleData.row, styleData.column, textField.text);
                }
            }
        }
        tableView.rowDelegate: Rectangle {
            height: 22
        }
    }
    Component.onCompleted: {
        //        console.log(pageId);
        //            homeStackView.push(xDeviceWindow)
        //            xDeviceWindow.pageId=2
    }
}

