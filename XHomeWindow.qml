﻿import QtQuick 2.0
import QtQuick.Controls 1.4
Rectangle {
    property int pageId: 0
    property bool isvisible: false
    height: parent.height
    width: parent.width
    anchors.top: parent.top
    anchors.left: parent.left
    anchors.topMargin: 20
    anchors.leftMargin: 40
    id : xHomeWindow
    color: "#B0C4DE"
    visible: isvisible
    XNavButton1 {
        id: videoViewBtn
        img: "image/video300x168.png"
        txt: qsTr("视频预览")
        navHeight: 168+2 + 22+3
        navWidth: 300+2+3
        index: 1
    }

    XNavButton1 {
        id: alarmDeviceBtn
        anchors.left: videoViewBtn.right
        anchors.leftMargin: 10
        img: "image/alarm300x168.png"
        txt: qsTr("设备管理")
        navHeight: 168 + 2 + 22+3
        navWidth: 300 + 2+3
        index: 2
    }

    XNavButton1 {
        id: alarmLinkBtn
        anchors.left: alarmDeviceBtn.right
        anchors.leftMargin: 10
        img: "image/als300x168.png"
        txt: qsTr("警情联动")
        navHeight: 168 + 2 + 22+3
        navWidth: 300 + 2+3
        index: 3
    }
    Component.onCompleted: {
//        console.log(pageId);
        //        homeStackView.push(xHomeWindow)
        //        xHomeWindow.pageId=0
    }
}


