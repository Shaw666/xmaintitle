﻿import QtQuick 2.0
import QtQuick.Controls 1.4
import QtQuick.Window 2.0

Rectangle {
    id: xMainTitle
    property int mainWidth: 120
    property int mainHeight: 36
    width: mainWidth
    height: mainHeight
    color: "#00BFFF"
    //    MenuBar {
    //        Menu {
    //            title: "文件"
    //            MenuItem{
    //                text: "第一"
    //            }
    //        }
    //    }

    MouseArea {
        anchors.fill: parent
        acceptedButtons: Qt.LeftButton
        property point clickPos: "0,0"
        onPressed: {
            clickPos = Qt.point(mouse.x, mouse.y)
            //            mouse.accepted = true
        }
        onPositionChanged: {
            //鼠标偏移量
            var delta = Qt.point(mouse.x - clickPos.x, mouse.y - clickPos.y)
            //如果mainwindow继承自QWidget,用setPos
            mainWindow.setX(mainWindow.x + delta.x)
            mainWindow.setY(mainWindow.y + delta.y)
        }
    }
    Label {
        anchors.right: mainTitleMin.left
        anchors.rightMargin: 10
        text: "net"
    }

    XMainTitleButton {
        id : mainTitleClose
        btnWidth: 16
        btnHeight: 16
        anchors.top: parent.top
        anchors.right: parent.right
        anchors.topMargin: 10
        anchors.bottomMargin: 10
        anchors.rightMargin: 15
        img: "image/close.png"

        MouseArea {
            anchors.fill: mainTitleClose
            hoverEnabled: true
            onEntered: {
                mainTitleClose.img = "image/close-red.png"
            }
            onExited: {
                mainTitleClose.img = "image/close.png"
            }
            onClicked: {
                Qt.quit()
            }
        }
    }

    XMainTitleButton {
        id : mainTitleMax
        width: 16
        height: 16
        anchors.top: parent.top
        anchors.right: mainTitleClose.left
        anchors.topMargin: 10
        anchors.bottomMargin: 10
        anchors.rightMargin: 10
        img: "image/max.png"
        MouseArea {
            anchors.fill: mainTitleMax
            hoverEnabled: true
            onEntered: {
                mainTitleMax.img = "image/max-white.png"
            }
            onExited: {
                mainTitleMax.img = "image/max.png"
            }
            onClicked: {
                if (mainWindow.visibility === Window.Windowed) {
                    mainWindow.showFullScreen()
                }else{
                    mainWindow.showNormal()
                }
            }
        }
    }

    XMainTitleButton {
        id : mainTitleMin
        width: 16
        height: 16
        anchors.top: parent.top
        anchors.right: mainTitleMax.left
        anchors.topMargin: 10
        anchors.bottomMargin: 10
        anchors.rightMargin: 10
        img: "image/min.png"
        MouseArea {
            anchors.fill: mainTitleMin
            hoverEnabled: true
            onEntered: {
                mainTitleMin.img = "image/min-white.png"
            }
            onExited: {
                mainTitleMin.img = "image/min.png"
            }
            onClicked: {
                mainWindow.showMinimized()
            }
        }
    }
}
