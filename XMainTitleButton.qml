﻿import QtQuick 2.0

Rectangle {
    property alias img: image.source
    property int btnWidth: 16
    property int btnHeight: 16

    width: btnWidth
    height: btnHeight
    signal btnClicked()
    Image {
        id: image
    }
}
