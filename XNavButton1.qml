﻿import QtQuick 2.0
import QtQuick.Controls 1.4
import QtGraphicalEffects 1.0
//主页功能选项按钮
Rectangle {
    property alias img:image.source ;
    property alias txt:navName.text ;
    property int navHeight: 100
    property int navWidth: 100
    property int index: 0;

    property int shadowH: 2
    property int shadowV: 2

    height: navHeight
    width: navWidth
    color: "#B0C4DE"

    MouseArea {
        anchors.fill: parent
        hoverEnabled: true
        onEntered: {
            shadowH=5
            shadowV=5
        }
        onExited: {
            shadowH=2
            shadowV=2
        }
        onClicked: {
            navWindowTabs.createTab(index, navName.text)
        }
    }

    Rectangle{
        id: xNavButton1
        height: navHeight-2
        width: navWidth-2
        Image {
            id: image
            height: navHeight-2-22
            width: navWidth-2
            anchors.left: parent.left
            anchors.top: parent.top
        }
        Label{
            id: navName
            anchors.bottom: parent.bottom
            anchors.bottomMargin: 0
            anchors.horizontalCenter: parent.horizontalCenter
            font.pixelSize: 22
            font.italic: true
            color: "#4682B4"
        }
    }
    DropShadow {
        anchors.fill: xNavButton1
        horizontalOffset: shadowH
        verticalOffset: shadowV
        radius: 8.0
        samples: 17
        color: "#4682B4"
        source: xNavButton1
    }
}
