﻿import QtQuick 2.7
import QtQuick.Controls 1.4
//上方导航栏界面
Rectangle {
    property alias txt: navTitleName.text
    property int navTitleHeight: 36
    property int navTitleWidth: 120
    property int shadowH: 0
    property int shadowV: 1
    property int index: 0
    property bool isvisible: true
    color: navWindow.color
    id: xNavTitleButton
    width: navTitleWidth
    height: navTitleHeight-2
    //    signal ManualCreateMainWindow()
    Label {
        id: navTitleName
        anchors.centerIn: parent
    }
    Rectangle {
        id: navTitleDownBorder
        width: navTitleWidth
        height: 2
        anchors.bottom: parent.bottom
        color: "#FF69B4"
        visible: isvisible
    }
    MouseArea {
        id: mainArea
        anchors.fill: parent
        hoverEnabled: true
        onEntered: {
            xNavTitleButton.color = "#87CEEB"
            if(index !== 0){
                navTitleClose.visible = true}
        }
        onExited: {
            xNavTitleButton.color = "#00BFFF"
            navTitleClose.visible = false
        }
        onClicked: {
            //创建功能主页面
            if(navWindowTabs.currTabId !== index){
                navWindowTabs.spriteTabs[navWindowTabs.currTabId].isvisible = false
                isvisible=true
                navWindowTabs.currTabId = index
            }

            homeStackView.createMainWindow(index, navTitleName.text)
        }
    }

    Image {
        id: navTitleClose
        width: 32
        height: 32
        anchors.top: parent.top
        anchors.right: parent.right
        source: "image/tabclose32x32.png"
        visible: false
        MouseArea {
            anchors.fill: parent
            hoverEnabled: false
            onClicked: {
                console.log("destroy nav title ", navTitleName.text, index)
                navWindowTabs.spriteTabs[index] = null
                homeStackView.destroyMainWindow(index)
                xNavTitleButton.destroy();
                navWindowTabs.spriteTabs[0].isvisible = true
            }
        }
    }

    Component.onCompleted: {
        console.log("nav title completed", navTitleName.text, index)
        emit: mainArea.clicked(null);
    }
}
