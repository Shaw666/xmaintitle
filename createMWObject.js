﻿var component;
var sourceFiles = ["XHomeWindow.qml", "", "XDeviceWindow.qml", "", ""]

function createMainWindowObject(index) {
    if(homeStackView.spriteMainWindows[index]){
        console.log("object already exists")
        return;
    }
    if(sourceFiles[index] === "" || 0 > index || 4 < index){
    console.log("no qml file to create component")
    }
    component = Qt.createComponent(sourceFiles[index]);
    if (component.status === Component.Ready)
        finishCreation(index);
    else
        component.statusChanged.connect(finishCreation);
}
//0-homewindow 1-videoview 2-devicemanage
function finishCreation(index) {
    if (component.status === Component.Ready) {
        switch(index) {
        case 0 : homeStackView.spriteMainWindows[index] =
                 component.createObject(homeStackView, {pageId:index, isvisible: false})
            break;
        case 1 : break;
        case 2 : homeStackView.spriteMainWindows[index] =
                 component.createObject(homeStackView, {pageId:index, isvisible: false})
            break;
        case 3 : break;
        case 4 : homeStackView.spriteMainWindows[index] = component.createObject(homeStackView, {alarmWindowState:2});
            if (homeStackView.spriteMainWindows[index] === null) {
                console.log("Error creating object");
            }
            break;
        default : break;
        }
    } else if (component.status === Component.Error) {
        // Error Handling
        console.log("Error loading component:", component.errorString());
    }
}
