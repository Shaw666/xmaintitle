﻿var component;

function createSpriteObjects(index, title) {
    if(navWindowTabs.spriteTabs[index]){
        console.log("object already exists")
        return;
    }
    component = Qt.createComponent("XNavTitleButton.qml");
    if (component.status === Component.Ready)
        finishCreation(index, title);
    else
        component.statusChanged.connect(finishCreation);
}

function finishCreation(index, title) {
    if (component.status === Component.Ready) {
        navWindowTabs.spriteTabs[index] = component.createObject(navWindowTabs, {navTitleHeight:36, navTitleWidth:120, txt:title, index:index});
        if (navWindowTabs.spriteTabs[index] === null) {
            // Error Handling
            console.log("Error creating object");
        }
    } else if (component.status === Component.Error) {
        // Error Handling
        console.log("Error loading component:", component.errorString());
    }
}
