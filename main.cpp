﻿#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQmlContext>
#include "qmlTableViewModel.h"

int main(int argc, char *argv[])
{
  QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);

  QGuiApplication app(argc, argv);

  QQmlApplicationEngine engine;

  QmlTableViewModel alarmModel(11);
  QVector<QString> list;
  list.push_back("2019-01-20");
  list.push_back("20:49:00");
  list.push_back("00001000");
  list.push_back("前台侧门-1000");
  list.push_back("窃盗");
  list.push_back("窃盗恢复");
  list.push_back("E130");
  list.push_back("001");
  list.push_back("01");
  list.push_back("COWN-U99-1A-298");
  list.push_back("前台侧门");

  alarmModel.Add(list);
  engine.rootContext()->setContextProperty("theModel", &alarmModel);

  QmlTableViewModel deviceManageModel(21);
  list.clear();
  for(int i=0; i<21; i++){
      list.push_back(QString("%1").arg(i));
    }
  engine.rootContext()->setContextProperty("deviceModel", &deviceManageModel);

  engine.load(QUrl(QStringLiteral("qrc:/main.qml")));
  if (engine.rootObjects().isEmpty())
    return -1;

  return app.exec();
}
