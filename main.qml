﻿import QtQuick 2.7
import QtQuick.Window 2.2
import QtQuick.Controls 2.0
import QtQuick.Layouts 1.3

import QtQuick.Controls.Styles 1.4
import QtQml.Models 2.3
import QtQuick.Controls.Material 2.0
import QtQuick.Controls.Material 2.0
import QtQuick.Controls.Universal 2.0
import "createSpriteObjects.js" as MyScript
import "createMWObject.js" as CreateMainWindowJS

ApplicationWindow {
    id : mainWindow
    visible: true
    minimumWidth: 1024
    minimumHeight: 768
    title: qsTr("Hello World")
    flags: Qt.Window | Qt.FramelessWindowHint
    color: "#B0C4DE"
    XMainTitle {
        id : mainTitle
        mainWidth: 120
        mainHeight: 36
        anchors.top: parent.top
        anchors.right: parent.right
    }

    Rectangle {
        id: navWindow
        width: parent.width - mainTitle.width
        height: 36
        anchors.top: parent.top
        anchors.left: parent.left
        color: "#00BFFF"
        Row {
            id: navWindowTabs
            property var spriteTabs: new Array(4);
            property int currTabId: 0
            anchors.left: parent.left
            anchors.top: parent.top
            spacing: 1
            Component.onCompleted: createTab(0, qsTr("首页"))
//            XNavTitleButton {
//                id:homeTitleNav
//                navTitleHeight: 36
//                navTitleWidth: 120
//                index: 0
//                txt: qsTr("首页")
//                isvisible: true
//            }
            function createTab(index, title){
                console.log("create tab ", index, title)
                MyScript.createSpriteObjects(index, title);
            }
        }
    }

    Item {
        id: homeStackView
        width: parent.width
        height: parent.height-mainTitle.height-alarmWindow.height
        anchors.top: navWindow.bottom
        anchors.left: parent.left
        property var spriteMainWindows: new Array(4);
        property int currPageId: 0
        function createMainWindow(index){
            console.log("当前页面 ", homeStackView.currPageId)
            console.log("创建页面 ", index, homeStackView.spriteMainWindows[index])
            CreateMainWindowJS.createMainWindowObject(index)
            var currIndex = homeStackView.currPageId
            if(index !== currIndex){
                homeStackView.spriteMainWindows[currIndex].isvisible = false
                homeStackView.spriteMainWindows[index].isvisible = true
                homeStackView.currPageId = index;
            }else{
                homeStackView.spriteMainWindows[index].isvisible = true
            }
        }

        function destroyMainWindow(index){
            console.log("destroy mainWindow", index)
            homeStackView.spriteMainWindows[index].destroy()
            homeStackView.spriteMainWindows[index] = null
            homeStackView.spriteMainWindows[0].isvisible = true
        }
    }

    XAlarmWindow {
        id:alarmWindow
        mainHeight: 20
        alarmWindowState: 0
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.bottom: parent.bottom
        color: "#4682B4"
    }
}
