﻿#include "qmlTableViewModel.h"
#include <QDebug>

QmlTableViewModel::QmlTableViewModel(int roleCount)
  : QAbstractTableModel(nullptr)
{
  m_aryData.clear();
  this->roleCount = roleCount;
}

int QmlTableViewModel::rowCount(const QModelIndex &parent) const
{
  Q_UNUSED(parent)
  return m_aryData.size();
}

int QmlTableViewModel::columnCount(const QModelIndex &parent) const
{
  Q_UNUSED(parent)
  if(m_aryData.isEmpty()){
      return 0;
    }
  return m_aryData.at(0).size();
}

QVariant QmlTableViewModel::data(const QModelIndex &index, int role) const
{
  return m_aryData[index.row()][role];
}

QHash<int, QByteArray> QmlTableViewModel::roleNames() const
{
  QHash<int, QByteArray> roles;
  for(int i=0; i<roleCount; i++){
      roles[i] = QString("%1").arg(i).toLatin1();
    }
  return roles;
}

void QmlTableViewModel::Add(QVector<QString> list)
{
  beginInsertRows(QModelIndex(), m_aryData.size(), m_aryData.size()+1);
  m_aryData.push_back(list);
  endInsertRows();
}

void QmlTableViewModel::Set(int row, int column, QString text)
{
  if (row == -1){return;}
  if (column == -1){return;}
  beginResetModel();
  m_aryData[row][column] = text;
  endResetModel();
}

void QmlTableViewModel::Del()
{
  if (m_aryData.size() <= 0) return;
  beginRemoveRows(QModelIndex(), m_aryData.size() - 1, m_aryData.size() - 1);
  m_aryData.removeLast();
  endRemoveRows();
}

void QmlTableViewModel::Refresh()
{
  beginResetModel();
  endResetModel();
}

int QmlTableViewModel::getRoleCount() const
{
  return roleCount;
}

void QmlTableViewModel::setRoleCount(int value)
{
  roleCount = value;
}
