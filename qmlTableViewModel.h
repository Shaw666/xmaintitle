﻿#pragma once
#include <QAbstractTableModel>
#include <QVector>
#include <QString>
#pragma execution_character_set("utf-8")

class QmlTableViewModel : public QAbstractTableModel
{
    Q_OBJECT
public:
    explicit QmlTableViewModel(int roleCount);
    int rowCount(const QModelIndex & parent = QModelIndex()) const Q_DECL_OVERRIDE;
    int columnCount(const QModelIndex &parent = QModelIndex()) const Q_DECL_OVERRIDE;
    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const Q_DECL_OVERRIDE;
    QHash<int, QByteArray> roleNames() const Q_DECL_OVERRIDE;

    Q_INVOKABLE void Add(QVector<QString> list);
    Q_INVOKABLE void Set(int row, int column, QString text);
    Q_INVOKABLE void Del();
    Q_INVOKABLE void Refresh();
    int getRoleCount() const;
    void setRoleCount(int value);

private:
    QVector<QVector<QString>> m_aryData;
    int roleCount;
};
